/**
 * Created by yangdeng on 16/8/25.
 */
'use strict';
(function () {

    var validateService = function () {

        /**
         * @description 去除空格
         * @param str
         * @returns {*}
         */
        this.trim = function (str) {
            if ((str && str != '') || (str == 0 || str == '0')) {
                str = str.toString();
                str = str.replace(/^(\s|\u00A0)+/, '');
                for (var i = str.length - 1; i >= 0; i--) {
                    if (/\S/.test(str.charAt(i))) {
                        str = str.substring(0, i + 1);
                        break;
                    }
                }
                return str;
            }
            else {
                return '';
            }
        };


        /**
         * @description 验证手机号是否合法
         * @param str
         * @returns {*}
         */
        this.matchPhoneNum = function (str) {
            if (str || str != '' || (str == 0 && str == '0')) {
                var reg = /^1[3,7,5,8]\d{9}$/;
                str = this.trim(str);
                if (str != '') {
                    if (reg.test(str)) {
                        return str;
                    }
                    else {
                        return -1;
                    }
                }
                else {
                    return '';
                }
            }
            else {
                return '';
            }
        };


        /**
         * @description 验证只含数字
         * @param str
         * @returns {*}
         */
        this.validateOnlyDigst = function (str) {
            if (str || str != '' || (str == 0 && str == '0')) {
                var reg = /^[0-9]*$/;
                if (str != '') {
                    if (reg.test(str)) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        };

        /**
         * @description 验证邮箱格式
         * @param str
         * @returns {boolean}
         */
        this.validateEmailType = function (str) {
            if (str || str != '' || (str == 0 && str == '0')) {
                var reg =  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (str != '') {
                    if (reg.test(str)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
    };

    validateService.$Inject = [];

    angular.module('myApp.validate.service',[])
        .service('validateService',validateService);

})();