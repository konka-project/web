/**
 * Created by yangdeng on 16/8/24.
 */
'use strict';
var staticFn = {};

/***************************** Toast **********************************/

/**
 * @description toast提示框
 * @param message 消息体
 * @param duration 持续时间
 * @param options 参数 {position: String, animation: Array}
 * @param bkColor 背景颜色
 */
staticFn.toast = function (message, duration, options, type) {
    // 默认值设置
    var defaultBodyClass = 'xform-body animated clearfix';
    var defaultPosition = 'center';
    var defaultAnimation = ['fadeIn', 'fadeOut'];

    // 获取用户配置属性值
    if (options) {
        if (options.position) {
            defaultPosition = options.position;
        }
        if (options.animation) {
            defaultAnimation = options.animation;
        }
    }

    // 创建toast panel
    var backdrop = document.createElement('div');
    var toast = document.createElement('div');
    var body = document.createElement('div');
    var span = document.createElement('span');
    var icon = document.createElement('i');
    backdrop.setAttribute('class', 'xform-backdrop');
    toast.setAttribute('class', 'xform-toast');
    body.setAttribute('class', defaultBodyClass);
    span.setAttribute('class', 'text-area');
    span.innerHTML = message;

    body.className = defaultBodyClass + ' ' + defaultAnimation[0];
    backdrop.appendChild(toast);
    toast.appendChild(body);
    body.appendChild(span);
    body.appendChild(icon);
    document.body.appendChild(backdrop);

    switch (type) {
        case 'success':
            icon.setAttribute('class', 'alert-success-i');
            break;
        case 'error':
            icon.setAttribute('class', 'alert-error-i');
            break;
        case 'warning':
            icon.setAttribute('class', 'alert-warning-i');
            break;
        case 'info':
            icon.setAttribute('class', 'alert-info-i');
            break;
    }


    // 计算位置
    if (defaultPosition === 'center') {
        body.style.left = '50%';
        body.style.top = backdrop.clientHeight * 0.3 + 'px';
        body.style.marginLeft = -body.clientWidth / 2 + 'px';
    }
    if (defaultPosition === 'right-top') {
        body.style.top = '20px';
        body.style.right = '20px';
    }
    // end

    setTimeout(function () {
        body.className = defaultBodyClass + ' ' + defaultAnimation[1];
        setTimeout(function () {
            backdrop.remove();
        }, 700);
    }, duration);
};

Function.prototype.success = function (message, duration, options) {
    this(message, duration, options, 'success');
};

Function.prototype.error = function (message, duration, options) {
    this(message, duration, options, 'error');
};

Function.prototype.warning = function (message, duration, options) {
    this(message, duration, options, 'warning');
};

Function.prototype.info = function (message, duration, options) {
    this(message, duration, options, '#info');
};

staticFn.alert = function (message,title) {
    var p = $.Deferred();

    //设置蒙板样式
    var backdropClass = 'xForm-alert-backdrop';
    var alertBodyClass = 'xForm-alert-body animated time-300 centerIn';
    var alertHeaderClass = 'xForm-alert-header clearfix';
    var alertMainClass = 'xForm-alert-main';
    var alertFooterClass = 'xForm-alert-footer';
    var alertHeaderIconClass = 'fa fa-close';

    var alertBackdrop = document.createElement('div');
    var alertBody = document.createElement('div');
    var alertHeader = document.createElement('div');
    var alertMain = document.createElement('div');
    var alertFooter = document.createElement('div');
    var alertHeaderSpan = document.createElement('span');
    var alertHeaderIcon = document.createElement('i');
    var alertMainSpan = document.createElement('span');
    var alertFooterButton = document.createElement('button');

    alertBackdrop.setAttribute('class', backdropClass);
    alertBody.setAttribute('class', alertBodyClass);
    alertHeader.setAttribute('class', alertHeaderClass);
    alertMain.setAttribute('class', alertMainClass);
    alertFooter.setAttribute('class', alertFooterClass);
    alertHeaderIcon.setAttribute('class',alertHeaderIconClass);

    alertHeaderSpan.innerHTML = (title)?(title):('提示');
    alertMainSpan.innerHTML = (message)?(message):('这是提示内容');
    alertFooterButton.innerHTML = '确定';

    alertHeader.appendChild(alertHeaderSpan);
    alertHeader.appendChild(alertHeaderIcon);

    alertMain.appendChild(alertMainSpan);

    alertFooter.appendChild(alertFooterButton);

    alertBody.appendChild(alertHeader);
    alertBody.appendChild(alertMain);
    alertBody.appendChild(alertFooter);
    alertBackdrop.appendChild(alertBody);
    document.body.appendChild(alertBackdrop);

    alertHeaderIcon.onclick = function () {
        alertBody.className = 'xForm-alert-body animated time-300 fadeOut';
        setTimeout(function(){
            alertBackdrop.remove();
            p.resolve();
        },300);
    };

    alertFooterButton.onclick = function () {
        alertBody.className = 'xForm-alert-body animated time-300 fadeOut';
        setTimeout(function(){
            alertBackdrop.remove();
            p.resolve();
        },300);
    };

    return p.promise();
};

/***************************** End **********************************/

/************************* confirm modal ***************************/

/**
 * @description confirm对话框
 * @param options: {content: String, multiChose: Boolean} content: 对话框内容信息 multiChose: 是否有取消按钮
 * @return 返回一个promise 确定: 1, 取消: 0
 */
staticFn.confirm = function (options) {
    var p = $.Deferred();

    var backdrop = document.createElement('div');
    var modal = document.createElement('div');
    var header = document.createElement('div');
    var body = document.createElement('div');
    var footer = document.createElement('div');

    var ok = document.createElement('span');
    var cancel = document.createElement('span');

    backdrop.setAttribute('class', 'xform-confirm-backdrop static');
    modal.setAttribute('class', 'xform-confirm-modal animated');
    header.setAttribute('class', 'xform-confirm-header');
    body.setAttribute('class', 'xform-confirm-body');
    footer.setAttribute('class', 'xform-confirm-footer');
    ok.setAttribute('class', 'xform-btn xform-ok');
    cancel.setAttribute('class', 'xform-btn xform-cancel');
    ok.innerHTML = '确定';
    cancel.innerHTML = '取消';

    modal.className = 'xform-confirm-modal animated time-300 centerIn';

    var headerContent = [
        '<span class="tip">',
        '提示',
        '</span>'
    ];

    var bodyContent = [
        '<div class="xform-modal-content"><span>',
        options.content,
        '</span></div>'
    ];

    ok.onclick = function () {
        modal.className = 'xform-confirm-modal animated time-300 fadeOut';
        p.resolve(1);

        setTimeout(function () {
            backdrop.remove();
            ok.onclick = null;
        }, 300);
    };

    cancel.onclick = function () {
        modal.className = 'xform-confirm-modal animated time-300 fadeOut';
        p.resolve(0);

        setTimeout(function () {
            backdrop.remove();
            cancel.onclick = null;
        }, 300);
    };

    header.innerHTML = headerContent.join('');
    body.innerHTML = bodyContent.join('');

    modal.appendChild(header);
    modal.appendChild(body);
    modal.appendChild(footer);
    footer.appendChild(ok);

    if (options.multiChose) {
        footer.appendChild(cancel);
    }

    backdrop.appendChild(modal);
    document.body.appendChild(backdrop);

    return p.promise();
};

/***************************** End **********************************/