'use strict';
var myApp = angular.module('myApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngDragDrop',
    'ui.select2',
    'mgcrea.ngStrap',
    'ui.sortable',
    'gridster',
    'selectize',
    'ui.bootstrap',
    'ngImgCrop',
    
    'myApp.configs',
    'myApp.utillities',
    // 'myApp.constants', // MISC
    // 'myApp.directives',
    // 'myApp.filters', // MISC
    'myApp.services', // Services
    'myApp.directives',
    'myApp.controllers' // controller
]);