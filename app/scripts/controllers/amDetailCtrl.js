/**
 * Created by yangdeng on 16/8/25.
 */
'use strict';
(function () {

    var amDetailCtrl = function ($rootScope, $scope, validateService, deleteAccountSure, $state,chooseChinaCityModal) {

        $rootScope.chooseValue = 'am';

        $scope.chooseType = 'detail';

        var detailData = $scope.detailData = {};
        detailData.email = '2986789@qq.com';
        detailData.mobile = '18301125678';

        detailData.isEditEmail = false;
        detailData.isEditMobile = false;
        detailData.isEditPwd = false;
        detailData.rootTopList = [];
        detailData.logList = [];
        detailData.opList = [
            {
                name: '登录应用',
                value: 'login'
            },
            {
                name: '修改密码',
                value: 'updatePwd'
            },
            {
                name: '找回密码',
                value: 'findPwd'
            },
            {
                name: '编辑资料',
                value: 'editInfo'
            },
            {
                name: '邮箱绑定',
                value: 'bindEmail'
            },
            {
                name: '应用授权',
                value: 'appRoot'
            }
        ];

        detailData.areaData = {};


        $scope.init = function () {
            for (var i = 0; i < 20; i++) {
                detailData.rootTopList.push(i);
            }
            for (var j = 0; j < 50; j++) {
                detailData.logList.push(j);
            }
        };

        $scope.fn = {
            changeTag: function (e, type) {
                e.stopPropagation();
                $scope.chooseType = type;
            },
            editEmail: function (e) {
                e.stopPropagation();
                detailData.isEditEmail = true;
                detailData.email_e = angular.copy(detailData.email);
            },
            focusEmail: function () {
                detailData.emailError = false;
            },
            focusMobile: function () {
                detailData.mobileError = false;
            },
            focusPwd: function () {
                detailData.pwdError = false;
            },
            editMobile: function (e) {
                e.stopPropagation();
                detailData.isEditMobile = true;
                detailData.mobile_e = angular.copy(detailData.mobile);
            },
            resetPwd: function (e) {
                e.stopPropagation();
                detailData.new_pwd = 'hahadasb';
                detailData.isEditPwd = true;
            },
            saveDetailEmail: function (e) {
                e.stopPropagation();
                if (detailData.email_e && detailData.email_e.trim()) {
                    var res = validateService.validateEmailType(detailData.email_e);
                    if (res) {
                        detailData.email = validateService.trim(detailData.email_e);
                    }
                    else {
                        detailData.emailError = true;
                        detailData.emailErrorTip = '邮箱格式不正确';
                        return;
                    }
                }
                else {
                    detailData.email = '';
                }
                detailData.isEditEmail = false;
            },
            cancelEditEmail: function (e) {
                e.stopPropagation();
                detailData.emailError = false;
                detailData.isEditEmail = false;
            },
            saveDetailMobile: function (e) {
                e.stopPropagation();
                if (detailData.mobile_e && detailData.mobile_e.trim()) {
                    var mobile_e = validateService.trim(detailData.mobile_e);
                    if (mobile_e.length < 11) {
                        detailData.mobileError = true;
                        detailData.mobileErrorTip = '手机号格式不正确';
                        return;
                    }
                    else {
                        var res = validateService.matchPhoneNum(detailData.mobile_e);
                        if (res) {
                            detailData.mobile = mobile_e;
                        }
                        else {
                            detailData.mobileError = true;
                            detailData.mobileErrorTip = '手机号格式不正确';
                            return;
                        }
                    }
                }
                else {
                    detailData.mobileError = true;
                    detailData.mobileErrorTip = '手机号不能为空';
                    return;
                }
                detailData.isEditMobile = false;
            },
            cancelMobile: function (e) {
                e.stopPropagation();
                detailData.mobileError = false;
                detailData.isEditMobile = false;
            },
            saveEditPwd: function (e) {
                e.stopPropagation();
                if (detailData.new_pwd) {
                    if (detailData.new_pwd.length < 6) {
                        detailData.pwdError = true;
                        detailData.pwdErrorTip = '密码长度不得小于6';
                        return;
                    }
                    else {
                        if (validateService.validateOnlyDigst(detailData.new_pwd)) {
                        }
                        else {
                            detailData.pwdError = true;
                            detailData.pwdErrorTip = '密码不能为纯数字';
                            return;
                        }
                    }
                }
                else {
                    detailData.pwdError = true;
                    detailData.pwdErrorTip = '密码不能为空';
                    return;
                }
                detailData.isEditPwd = false;
            },
            cancelPwd: function (e) {
                e.stopPropagation();
                detailData.isEditPwd = false;
                detailData.pwdError = false;
            },

            closeAccount: function (e) {
                e.stopPropagation();
                deleteAccountSure.open('确定要禁用此用户？', '被禁用后的帐号无法登录统一帐号系统，管理员可重新启用该用户。', '禁用成功');
            },
            deleteAccount: function (e) {
                e.stopPropagation();
                deleteAccountSure.open('确定要删除此用户？', '删除用户后，所有用户信息均失效，采用标记删除方式，数据库中依然保留用户信息。', '删除成功').then(function () {
                    $state.go('am');
                });
            },
            chooseSignUpData: function (e, targetData ,typeName) {
                e.stopPropagation();
                chooseChinaCityModal.open(targetData,typeName);
            },
            goBack:function (e) {
                e.stopPropagation();
                $state.go('am');
            }
        };

        $scope.init();

    };

    amDetailCtrl.$inject = ['$rootScope', '$scope', 'validateService', 'deleteAccountSure', '$state','chooseChinaCityModal'];

    angular.module('account.manage.detail.controller', [])
        .controller('amDetailCtrl', amDetailCtrl)

})();