/**
 * Created by yangdeng on 16/8/22.
 */
'use strict';
(function () {

    angular.module('myApp.controllers',[
        'myApp.account.manage.controller',
        'myApp.app.manage.controller',
        'myApp.data.manage.controller',
        'myApp.root.manage.controller',
        'myApp.system.manage.controller',
        'test.controller',

        'account.manage.detail.controller',
        'app.manage.add.controller',
        'app.edit.controller'
    ]);

})();