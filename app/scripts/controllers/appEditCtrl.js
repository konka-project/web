/**
 * Created by yangdeng on 16/8/29.
 */
'use strict';
(function () {

    var appEditCtrl = function ($scope, $rootScope, chooseImage, $state) {

        $rootScope.chooseValue = 'appm';
        var appData = $scope.appData = {};
        appData.times = 1;

        appData.timeData = [
            {
                name: '一周',
                value: 1
            },
            {
                name: '一月',
                value: 2
            },
            {
                name: '一年',
                value: 3
            },
            {
                name: '永久',
                value: 4
            }
        ];

        appData.appType = 'phone';

        $scope.fn = {
            editApp: function (e) {
                e.stopPropagation();
                appData.isEdit = true;
            },
            save: function (e) {
                e.stopPropagation();
                appData.isEdit = false;
            },
            deleteApp: function (e) {
                e.stopPropagation();
                $state.go('appm');
            },
            uploadImg: function () {
                chooseImage.open(appData);
            },
            goBack: function () {
                $state.go('appm')
            }
        };
    };

    appEditCtrl.$inject = ['$scope', '$rootScope', 'chooseImage', '$state'];

    angular.module('app.edit.controller', [])
        .controller('appEditCtrl', appEditCtrl);

})();