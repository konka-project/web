/**
 * Created by yangdeng on 16/8/24.
 */
'use strict';
(function () {

    var testCtrl = function ($timeout,$scope,chooseChinaCityModal) {


        var data = $scope.data = {};

        $scope.aaa = function () {
            chooseChinaCityModal.open(data);
        };

    };

    testCtrl.$inject = ['$timeout','$scope','chooseChinaCityModal'];

    angular.module('test.controller',[])
        .controller('testCtrl',testCtrl);

})();