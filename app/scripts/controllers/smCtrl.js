/**
 * Created by yangdeng on 16/8/23.
 */
'use strict';
(function () {

    var smCtrl = function ($rootScope) {
        $rootScope.chooseValue = 'sm';
    };

    smCtrl.$inject = ['$rootScope'];

    angular.module('myApp.system.manage.controller',[])
        .controller('smCtrl',smCtrl);

})();