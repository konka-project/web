/**
 * Created by yangdeng on 16/8/26.
 */
'use strict';
(function () {

    var appAddCtrl = function ($scope,chooseImage, $state,$rootScope) {
        $rootScope.chooseValue = 'appm';
        var appData = $scope.appData = {};
        appData.appType = 'phone';
        $scope.imgUrl = null;


        $scope.myImage='';
        $scope.myCroppedImage='';

        $scope.fn = {
            uploadImg : function () {
                chooseImage.open(appData);
            },
            goBack: function () {
                $state.go('appm')
            }
        };

    };

    appAddCtrl.$inject = ['$scope','chooseImage','$state','$rootScope'];

    angular.module('app.manage.add.controller',[])
        .controller('appAddCtrl',appAddCtrl);

})();