/**
 * Created by yangdeng on 16/8/23.
 */
'use strict';
(function () {

    var dmCtrl = function ($rootScope) {
        $rootScope.chooseValue = 'dm';
    };

    dmCtrl.$inject = ['$rootScope'];

    angular.module('myApp.data.manage.controller',[])
        .controller('dmCtrl',dmCtrl);

})();