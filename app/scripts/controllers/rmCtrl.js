/**
 * Created by yangdeng on 16/8/23.
 */
'use strict';
(function () {

    var rmCtrl = function ($rootScope, $scope) {
        $rootScope.chooseValue = 'rm';

        var rootData = $scope.rootData = {};

        rootData.chooseType = 'admin';
        rootData.rootList = [];

        $scope.fn = {
            changeTag: function (e, type) {
                rootData.chooseType = type;
            }
        };

        for(var i = 0; i< 50 ; i++){
            rootData.rootList.push(i);
        }
    };

    rmCtrl.$inject = ['$rootScope', '$scope'];

    angular.module('myApp.root.manage.controller', [])
        .controller('rmCtrl', rmCtrl);

})();