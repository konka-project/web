/**
 * Created by yangdeng on 16/8/23.
 */
'use strict';
(function () {

    var appmCtrl = function ($rootScope, $scope, exportAccount, deleteAccountSure,$state) {
        $rootScope.chooseValue = 'appm';

        var appData = $scope.appData = {};

        appData.topList = [];

        $scope.init = function () {
            for (var i = 0; i < 50; i++) {
                appData.topList.push(i);
            }
        };

        $scope.fn = {
            exportAppData: function (e) {
                e.stopPropagation();
                exportAccount.open();
            },
            deleteAppData: function (e) {
                e.stopPropagation();
                deleteAccountSure.open('确定要删除该应用吗？', '删除后该app_id的所有授权信息均失效，不可继续访问系统。', '删除成功');
            },
            closeAppData: function (e, index) {
                e.stopPropagation();
                deleteAccountSure.open('确定要禁用该应用吗？', '禁用后该app_id的所有授权信息均失效，不可继续访问系统。', '禁用成功').then(function () {
                    if (index % 3 == 0) {
                        staticFn.toast.error('不能删除启用中的应用', 1500, {
                            position: 'center',
                            animation: ['fadeIn', 'fadeOut']
                        })
                    }
                    else {
                    }
                });
            },
            appDetail: function (e) {
                e.stopPropagation();
                $state.go('appm-edit');
            },
            createAppData: function (e) {
                e.stopPropagation();
                $state.go('appm-add');
            }
        };

        $scope.init();
    };

    appmCtrl.$inject = ['$rootScope', '$scope', 'exportAccount', 'deleteAccountSure','$state'];

    angular.module('myApp.app.manage.controller', [])
        .controller('appmCtrl', appmCtrl);

})();