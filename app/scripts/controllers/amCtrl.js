/**
 * Created by yangdeng on 16/8/22.
 */
'use strict';
(function () {

    /**
     * @description 账户管理controller
     * @param $rootScope
     * @param $scope
     * @param $timeout
     */
    var amCtrl = function ($rootScope, $scope, $timeout ,chooseChinaCityModal ,deleteAccountSure ,createAccount,exportAccount ,$state) {
        $rootScope.chooseValue = 'am';

        $scope.thisList = [];

        $scope.maxSize = 5;
        $scope.bigTotalItems = 175;
        $scope.bigCurrentPage = 1;
        $scope.isLoading = true;
        $scope.readyLoading = true;

        var amData = $scope.amData = {};
        amData.signUpAdd = {};
        amData.lastLoginData = {};

        amData.sexData = [{
            name: '男',
            value: 'man'
        },{
            name: '女',
            value: 'woman'
        }];

        amData.statusData = [{
            name: '启用',
            value: 'open'
        },{
            name: '禁用',
            value: 'close'
        }];

        amData.signUpData = [{
            name: '广东省深圳市',
            value: 1
        }];

        amData.lastLoginData = [{
            name: '广东省深圳市',
            value: 1
        }];



        $scope.fn = {
            deleteAccount: function (e) {
                e.stopPropagation();
                deleteAccountSure.open('确定要删除此用户？','删除用户后，所有用户信息均失效。','删除成功');
            },
            createAccount: function () {
                createAccount.open();
            },
            exportAccount:function () {
                exportAccount.open();
            },
            showDetail: function (e) {
                e.stopPropagation();
                $state.go('am-detail');
            }
        };

        /**
         * @description 初始化
         */
        $scope.init = function () {
            for (var i = 0; i < 35; i++) {
                $scope.thisList.push(i);
            }
        };

        /**
         * @description 选择地图
         * @param e
         * @param targetData
         * @param typeName
         */
        $scope.chooseSignUpData = function (e, targetData ,typeName) {
            e.stopPropagation();
            chooseChinaCityModal.open(targetData,typeName);
        };

        $scope.init();
    };

    amCtrl.$inject = ['$rootScope', '$scope', '$timeout','chooseChinaCityModal','deleteAccountSure','createAccount','exportAccount','$state'];

    angular.module('myApp.account.manage.controller', [])
        .controller('amCtrl', amCtrl);

})();