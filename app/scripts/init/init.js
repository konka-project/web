/**
 * Created by yangdeng on 16/8/23.
 */
'use strict';
(function () {

    var init = function ($rootScope,$cookies) {


        // staticFn.toast.success('删除成功', 1500, {
        //     position: 'center',
        //     animation: ['fadeIn', 'fadeOut']
        // });

        /**
         * @description 下拉框配置
         * @type {{create: boolean, valueField: string, labelField: string, maxItems: number, delimiter: string, placeholder: string, searchField: string, onInitialize: $rootScope.formSelectConfig.onInitialize}}
         */
        $rootScope.valueSelectConfig = {
            create: false,
            valueField: 'value',
            labelField: 'name',
            maxItems: 1,
            delimiter: '|',
            placeholder: '请选择',
            searchField: 'name',
            onInitialize: function (selectize) {
            }
        };

    };

    init.$inject = ['$rootScope','$cookies'];

    angular.module('myApp')
        .run(init);

})();