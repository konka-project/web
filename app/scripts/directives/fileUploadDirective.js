/**
 * Created by yangdeng on 16/8/26.
 */
'use strict';
(function () {

    var fileUpload = function ($http) {
        return {
            restrict: 'A',
            scope: {
                file: '=ngModel'
            },
            link: function (scope, element, attrs) {
                element.bind('change', function () {
                    scope.file = element[0].files[0]
                    scope.$apply();
                });
            }
        }
    };
    fileUpload.$inject = ['$http'];

    angular.module('file.upload.directive',[])
        .directive('fileUpload',fileUpload);
})();