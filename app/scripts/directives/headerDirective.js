/**
 * Created by yangdeng on 16/8/22.
 */
'use strict';
(function () {

    var headerCtrl = function () {

    };

    var header = function () {
        return {
            restrict: 'A',
            templateUrl: 'partials/directive/header.html',
            controller: 'headerCtrl'
        };
    };

    headerCtrl.$inject = [];
    header.$inject = [];

    angular.module('myApp.header.directive',[])
        .controller('headerCtrl',headerCtrl)
        .directive('header',header);

})();