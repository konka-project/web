/**
 * Created by yangdeng on 16/8/22.
 */
'use strict';
(function () {

    angular.module('myApp.directives',[
        'myApp.header.directive',
        'left.nav.directive',
        'file.upload.directive'
    ]);

})();