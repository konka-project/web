/**
 * Created by yangdeng on 16/8/22.
 */
'use strict';
(function () {

    var leftNavCtrl = function ($scope, $state, $rootScope) {

        $scope.chooseNav = function (e, type) {
            e.stopPropagation();
            if (type == $rootScope.chooseValue) {
            }
            else {
                $rootScope.chooseValue = type;
                $state.go(type);
            }
        };

    };

    var leftNav = function () {
        return {
            restrict: 'A',
            templateUrl: 'partials/directive/leftNav.html',
            controller: 'leftNavCtrl'
        };
    };

    leftNavCtrl.$inject = ['$scope', '$state', '$rootScope'];
    leftNav.$inject = [];

    angular.module('left.nav.directive', [])
        .controller('leftNavCtrl', leftNavCtrl)
        .directive('leftNav', leftNav);

})();