/**
 * Created by yangdeng on 16/8/24.
 */
'use strict';

(function () {

    angular.module('myApp.services',[
        'choose.china.city.modal',
        'alert.service',
        'need.input.admin.password',
        'delete.account.sure',
        'create.account.modal',
        'export.account.modal',
        'choose.image.modal'
    ])

})();