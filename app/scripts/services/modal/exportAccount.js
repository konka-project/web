/**
 * Created by yangdeng on 16/8/25.
 */
'use strict';
(function () {

    var exportAccount = function ($modal,$timeout) {

        this.open = function () {
            var _modal = $modal({
                templateUrl: 'partials/modal/exportAccount.html',
                backdrop: 'static',
                placement: 'center',
                show: true,
                keyboard: false,
                animation: 'am-fade',
                backdropAnimation: 'am-fade'
            });

            var scope = _modal.$scope;

            scope.ok = function () {
                $timeout(function () {
                    $("#uploadIframe").get(0).src = '../../../download/1.csv';
                    scope.$hide();
                    staticFn.toast.success('导出成功', 1500, {
                        position: 'center',
                        animation: ['fadeIn', 'fadeOut']
                    });
                }, 10);
            }

        }

    };

    exportAccount.$inject = ['$modal','$timeout'];

    angular.module('export.account.modal',[])
        .service('exportAccount',exportAccount);

})();