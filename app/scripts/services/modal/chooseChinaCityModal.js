/**
 * Created by yangdeng on 16/8/24.
 */
'use strict';
(function () {

    var chooseChinaCityModal = function ($modal) {

        this.open = function (targetData,type) {
            var _modal =  $modal({
                templateUrl: 'partials/modal/chooseChinaCityModal.html',
                backdrop: 'static',
                placement: 'center',
                show: true,
                keyboard: false,
                animation: 'am-fade',
                backdropAnimation: 'am-fade'
            });
            var scope = _modal.$scope;

            var _modalData = scope.modalData = {};

            scope.cityData = china_city_area_zip;

            scope.init = function () {
                if(targetData.modalData){
                    scope.modalData =_modalData = angular.copy(targetData.modalData);
                }
                else{
                    _modalData.cityData = scope.cityData;
                }
            };

            scope.reset = function (e, type) {
                switch (type){
                    case 'cp':
                        _modalData.cityData = scope.cityData;
                        _modalData.cp = null;
                        _modalData.cm = null;
                        _modalData.cd = null;
                        break;
                    case 'cm':
                        _modalData.cityData = angular.copy(_modalData.thisCmList);
                        _modalData.cm = null;
                        _modalData.cd = null;
                        break;
                    case 'cd':
                        _modalData.cd = null;
                        break;
                }
            };

            scope.chooseCity = function (e,item) {
                e.stopPropagation();
                if(_modalData.cp){
                    if(_modalData.cm){
                        _modalData.cd = item.name;
                    }
                    else{
                        _modalData.cm = item.name;
                        _modalData.cityData = item.child;
                        _modalData.thisCdList = item.child;
                    }
                }
                else{
                    _modalData.cp = item.name;
                    _modalData.thisCmList = item.child;
                    _modalData.cityData = item.child;
                }
            };

            scope.ok = function () {
                targetData.modalData = angular.copy(_modalData);
                targetData[type] = _modalData.cp + ((_modalData.cm)?(_modalData.cm):('')) + ((_modalData.cd)?(_modalData.cd):(''));
                scope.$hide();
            };

            scope.init();
        };
    };

    chooseChinaCityModal.$inject = ['$modal'];

    angular.module('choose.china.city.modal',[])
        .service('chooseChinaCityModal',chooseChinaCityModal);

})();