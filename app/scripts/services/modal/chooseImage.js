/**
 * Created by yangdeng on 16/8/26.
 */
'use strict';
(function () {

    var chooseImage = function ($modal,$timeout) {

        this.open = function (targetData) {

            var _modal = $modal({
                templateUrl: 'partials/modal/chooseImage.html',
                backdrop: 'static',
                placement: 'center',
                show: true,
                keyboard: false,
                animation: 'am-fade',
                backdropAnimation: 'am-fade'
            });

            var scope = _modal.$scope;
            var data = scope.data = {};

            $timeout(function () {
                scope.ResultImgEle =  angular.element('#resultImg');
            });


            scope.myImage='';
            scope.myCroppedImage='';
            scope.$watch('data.file',function () {
                if(data.file){
                    var file = data.file;
                    var reader = new FileReader();
                    reader.onload = function (evt) {
                        scope.$apply(function(scope){
                            scope.myImage=evt.target.result;
                        });
                    };
                    reader.readAsDataURL(file);
                }
            });

            scope.updateImg = function(){
                if(scope.ResultImgEle && scope.ResultImgEle[0].src){
                    scope.resultImg = scope.ResultImgEle[0].src;
                }
            };

            scope.ok = function () {
                $timeout(function () {
                    targetData.image = scope.resultImg;
                    scope.$hide();
                },300);
            };

        };

    };

    chooseImage.$inject = ['$modal','$timeout'];

    angular.module('choose.image.modal',[])
        .service('chooseImage',chooseImage);

})();