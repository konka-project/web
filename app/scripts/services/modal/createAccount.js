/**
 * Created by yangdeng on 16/8/24.
 */
'use strict';
(function () {

    var createAccount = function ($modal, $timeout, validateService) {

        this.open = function () {
            var _modal = $modal({
                templateUrl: 'partials/modal/createAccount.html',
                backdrop: 'static',
                placement: 'center',
                show: true,
                keyboard: false,
                animation: 'am-fade',
                backdropAnimation: 'am-fade'
            });

            var scope = _modal.$scope;

            var amData = scope.amData = {},
                modalData = scope.modalData = {};

            modalData.isCheckMobile = false;
            modalData.isReady = false;

            scope.fn = {
                nameBlur: function () {
                    if (amData.name && amData.name.trim()) {
                    }
                    else {
                        modalData.nameError = true;
                        modalData.nameErrorTip = '用户昵称不能为空';
                    }
                },
                inNameFocus: function () {
                    modalData.nameError = false;
                },
                mobileBlur: function () {
                    modalData.isCheckMobile = true;
                    modalData.isReady = true;
                    $timeout(function () {
                        modalData.isCheckMobile = false;
                        modalData.isReady = false;
                        if (amData.mobile && amData.mobile.trim()) {
                            var resMob = validateService.matchPhoneNum(amData.mobile);
                            if (resMob == -1) {
                                modalData.mobileError = true;
                                modalData.mobileErrorTip = '手机号格式不合法';
                            }
                            else {
                                amData.mobile = resMob;
                                modalData.mobileTrue = true;
                            }
                        }
                        else {
                            modalData.mobileError = true;
                            modalData.mobileErrorTip = '手机号不能为空';
                        }
                    }, 2000);
                },
                inMobileFocus: function () {
                    modalData.isCheckMobile = false;
                    modalData.isReady = false;
                    modalData.mobileError = false;
                    modalData.mobileTrue = false;
                },
                pwdBlur: function () {
                    if (amData.pwd && amData.pwd.trim()) {
                        if (amData.pwd.length < 6) {
                            modalData.pwdError = true;
                            modalData.pwdErrorTip = '密码长度不得低于6位';
                        }
                        else {
                            if (validateService.validateOnlyDigst(amData.pwd)) {
                            }
                            else {
                                modalData.pwdError = true;
                                modalData.pwdErrorTip = '密码不能为纯数字';
                            }
                        }
                    }
                    else {
                        modalData.pwdError = true;
                        modalData.pwdErrorTip = '密码不能为空';
                    }
                },
                inPwdFocus: function () {
                    modalData.pwdError = false;
                },
                changeVisible: function (e, status) {
                    e.stopPropagation();
                    $timeout(function () {
                        modalData.isNotVisible = status;
                        angular.element('#am_pwd').attr('type', (modalData.isNotVisible ? ('text') : ('password')));
                    });
                },
                checkData: function () {
                    var canSub = true,
                        putData = {};
                    if (amData.name && amData.name.trim()) {
                        putData.name = validateService.trim(amData.name);
                    }
                    else {
                        canSub = false;
                        modalData.nameError = true;
                        modalData.nameErrorTip = '用户昵称不能为空';
                    }
                    modalData.isCheckMobile = false;
                    modalData.isReady = false;
                    if (amData.mobile && amData.mobile.trim()) {
                        var resMob = validateService.matchPhoneNum(amData.mobile);
                        if (resMob == -1) {
                            canSub = false;
                            modalData.mobileError = true;
                            modalData.mobileErrorTip = '手机号格式不合法';
                        }
                        else {
                            modalData.mobileTrue = true;
                            putData.mobile = resMob;
                        }
                    }
                    else {
                        canSub = false;
                        modalData.mobileError = true;
                        modalData.mobileErrorTip = '手机号不能为空';
                    }
                    if (amData.pwd) {
                        if (amData.pwd.length < 6) {
                            canSub = false;
                            modalData.pwdError = true;
                            modalData.pwdErrorTip = '密码长度不得低于6位';
                        }
                        else {
                            if (validateService.validateOnlyDigst(amData.pwd)) {
                                putData.pwd = amData.pwd;
                            }
                            else {
                                canSub = false;
                                modalData.pwdError = true;
                                modalData.pwdErrorTip = '密码不能为纯数字';
                            }
                        }
                    }
                    else {
                        canSub = false;
                        modalData.pwdError = true;
                        modalData.pwdErrorTip = '密码不能为空';
                    }
                    putData.email = amData.email;
                    putData.isSendMsg = amData.isSendMsg;
                    return {canSub: canSub, putData : putData};
                },
                ok: function () {
                    var result =  this.checkData();
                    if(result.canSub){
                        scope.$hide();
                        staticFn.toast.success('新建成功', 1500, {
                            position: 'center',
                            animation: ['fadeIn', 'fadeOut']
                        });
                    }
                }
            }
        };

    };

    createAccount.$inject = ['$modal', '$timeout', 'validateService'];

    angular.module('create.account.modal', [])
        .service('createAccount', createAccount);

})();