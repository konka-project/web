/**
 * Created by yangdeng on 16/8/24.
 */
'use strict';
(function () {

    var deleteAccountSure = function ($modal,$q) {

        this.open = function (title,msg,sureTip) {
            var promise = $q.defer();
            var _modal =  $modal({
                templateUrl: 'partials/modal/deleteAccountSure.html',
                backdrop: 'static',
                placement: 'center',
                show: true,
                keyboard: false,
                animation: 'am-fade',
                backdropAnimation: 'am-fade'
            });

            var scope = _modal.$scope;

            scope.title = title;
            scope.msg = msg;

            scope.ok = function () {
                scope.$hide();
                staticFn.toast.success(sureTip, 1500, {
                    position: 'center',
                    animation: ['fadeIn', 'fadeOut']
                });
                promise.resolve();
            };

            return promise.promise;
        };

    };

    deleteAccountSure.$inject = ['$modal','$q'];

    angular.module('delete.account.sure',[])
        .service('deleteAccountSure',deleteAccountSure);

})();