/**
 * Created by yangdeng on 16/8/24.
 */
'use strict';
(function () {

    var alertService = function ($q) {

        var _self = this;

        /**
         * @description 简单的alert
         * @param msg
         */
        this.simpleAlert = function (msg) {
            swal(msg);
        };

        /**
         * @description 基本alert
         * @param title
         * @param msg
         */
        this.baseAlert = function (title,msg) {
            swal(title, msg);
        };


        /**
         * @description 提示alert
         * @param title
         * @param msg
         * @param type
         */
        this.tipAlert = function (title,msg,type) {
            swal(title,msg,type)
        };


        /**
         * @description confirm
         * @param title
         * @param msg
         * @param type
         * @param needCancel
         * @param btnMsg
         * @returns {promise.promise}
         */
        this.confirm = function (title,msg,type,needCancel,btnMsg) {
            var promise = $q.defer();
            swal({
                title: title,
                text: msg,
                type: type,
                showCancelButton: needCancel,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: btnMsg,
                closeOnConfirm: false
            }, function(){
                promise.resolve();
            });
            return promise.promise;
        };


        /**
         * @description 带结果的confirm
         * @param title
         * @param msg
         * @param type
         * @param needCancel
         * @param btnMsg
         * @param cancelMsg
         * @returns {promise.promise}
         */
        this.confirmWithRes = function (title,msg,type,needCancel,btnMsg,cancelMsg) {
            var promise = $q.defer();
            swal({
                title: title,
                text: msg,
                type: type,
                showCancelButton: needCancel,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: btnMsg,
                cancelButtonText: cancelMsg,
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm){
                promise.resolve(isConfirm);
            });
            return promise.promise;
        };

        /**
         * @description 带输入框的alert
         * @param title
         * @param msg
         * @param placeholder
         * @param blanckErrorMsg
         */
        this.alertWithInput = function (title,msg,placeholder,blanckErrorMsg) {
            swal({
                title: title,
                text: msg,
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: placeholder
            }, function(inputValue){
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError(blanckErrorMsg);
                    return false;
                }
                promise.resolve(inputValue);
            });
        };

        /**
         * @description 带异步请求的alert
         * @param title
         * @param msg
         * @param type
         * @param btnMsg
         * @param cancelMsg
         * @param promise
         * @param successTitle
         * @param successMsg
         */
        this.alertWithPromise = function (title,msg,type,btnMsg,cancelMsg,promise,successTitle,successMsg) {
            var pro = $q.defer();
            swal({
                title: title,
                text: msg,
                type: type,
                confirmButtonText: btnMsg,
                cancelButtonText: cancelMsg,
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function(){
                promise.then(function (res) {
                    console.log(res);
                    pro.resolve(res);
                    _self.tipAlert(res,successMsg,'success');
                });
            });
            return promise.promise;
        };
    };

    alertService.$inject = ['$q'];

    angular.module('alert.service',[])
        .service('alertService',alertService);

})();