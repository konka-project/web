/**
 * Created by yangdeng on 16/8/22.
 */
'use strict';
(function () {

    var routerConfig = function ($stateProvider,$urlRouterProvider) {
        $urlRouterProvider.otherwise('/am');

        $stateProvider
            .state('dm',{
                url: '/dm',
                templateUrl: 'partials/dataManage/dataManage.html',
                controller: 'dmCtrl'
            })
            .state('am',{
                url: '/am',
                templateUrl: 'partials/accountManage/accountManage.html',
                controller: 'amCtrl'
            })
            .state('appm',{
                url: '/appm',
                templateUrl: 'partials/appManage/appManage.html',
                controller: 'appmCtrl'
            })
            .state('rm',{
                url: '/rm',
                templateUrl: 'partials/rootManage/rootManage.html',
                controller: 'rmCtrl'
            })
            .state('sm',{
                url: '/sm',
                templateUrl: 'partials/systemManage/systemManage.html',
                controller: 'smCtrl'
            })
        
            .state('test',{
                url: '/test',
                templateUrl: 'partials/accountManage/test.html',
                controller: 'testCtrl'
            })

            .state('am-detail',{
                url: '/am-detail',
                templateUrl: 'partials/accountManage/accountDetail.html',
                controller: 'amDetailCtrl'
            })

            .state('appm-add',{
                url: '/app-add',
                templateUrl: 'partials/appManage/addAppManage.html',
                controller: 'appAddCtrl'
            })

            .state('appm-edit',{
                url: '/app-edit',
                templateUrl: 'partials/appManage/editAppManage.html',
                controller: 'appEditCtrl'
            });
    };

    routerConfig.$inject = ['$stateProvider','$urlRouterProvider'];

    angular.module('myApp.router.config',[])
        .config(routerConfig);

})();